# Devlog \# 3

Today Celosia and I (mostly Celosia, taking the lead on game design elements) finished coming up with unique commands for the companions and determining the unique passives they can unlock at the end of their upgrade paths.

We decided to just do one upgrade path per companion, with two branching paths that branch once more. You upgrade each companion up to three times.

I fiddled with C# in Godot a bit and realized changing from GDScript to C# is pretty hard in that context. I also just don't remember that much from my C# studies last year. So it's good that I already decided this week is mostly brushing up on C# while doing design work.

Last night I did some reading about interfaces in object-oriented programming and realized how perfect they are for what I'm trying to accomplish. It seems they were made for the Command pattern. Since interfaces are a feature in C# and not in GDScript, it's good that I already decided to use C# for performance reasons. Serendipitous.

Mostly I took a break today. My sleep is still bad and I was especially tired today. So I'll try to rest up and get back on top of things tomorrow.