# Devlog \# 1

My project is called Escorts Survivor. It's a Vampire Survivors clone made in Godot about a helpless survivor bossing around his super competent companions. The player moves around with WASD while using his or her mouse cursor to command AI companions to deal with the oncoming hordes of enemies.

I worked on a lot of brainstorming last night. I decided that this project's main goals are to learn a lot about AI and to implement the Command pattern in Godot.

It was apparent from the first that AI was important for this project, as the main gameplay loop involves manually commanding AI companions to move around and attack enemies as well as, time allowing, automating them to a small extent by setting parameters in their command menus. Think of Gambits from Final Fantasy XII.

As for the Command pattern, thoughts on that started when I was thinking about making the game multiplayer. I thought it would be fun to add local multiplayer where one or both players control companions. I also thought about a system early on where the enemy AI has their own version of survivors giving commands to their own version of companions. This could form fun and thematically strong bosses.

So I wanted to create an abstract controller system where actors in the game can be controlled either by a human player, an AI generating optimal commands, or a stream of commands from a (human or AI) survivor's command menu inputs. I did some research and learned that what I want is the Command pattern.

The original book on design patterns by the Gang of Four has this to say about the Command pattern: "Encapsulate a request as an object, thereby letting users parameterize clients with different requests, queue or log requests, and support undoable operations."

They later say: "Commands are an object-oriented replacement for callbacks." The phenomenal, free book Game Programming Patterns, where I learned about all this, thinks the latter is a better description. Basically, this pattern boils down to replacing functions with objects in the sense of object-oriented programming. You can use these objects to pass commands around between other objects or even pass the objects to be commanded around.

Excuse me if my explanation is poor as I only began reading about this pattern last night. I will be doing a lot of research on it over the next few days. That, along with brainstorming and modeling game logic in Mermaid, will make up the bulk of my activity tonight, tomorrow, and Wednesday. Then I'll begin prototyping proper in Godot and working steadily until the end of the jam on September 4th.

So what did I come up with last night? Nothing terribly interesting to you for now. I thought of a bunch of stats and status effects I would want interplaying with each other in a Survivors game of my own. I decided that having four companion slots, six possible companions to find during a run, each with three or four upgrade paths with two or three branches each, and no upgrades whatsoever for the companion would suit the game well. I also came up with some personality for the survivor and his six companions. I'll tell you about them now and show you my awful programmer art.

The first is Hank, the survivor. He's arrogant and impatient and needs that money, he really does, he needs that money to pay his bills.

Next is Brynhilda, the starting companion, and therefore the most loyal and exasperated companion. (Note: you can unlock the ability to set your starting companion as someone else eventually, at least in the hypothetical full game.) She wields an axe which slices in a wide arc.

Then there's Diego the brawler. He's kind of crazy and very intense. He's fast with his fists and can carry the survivor via his command menu, able to give temporary invincibility and then what amounts to a teleport by tossing the survivor somewhere on-screen. In this game, it's important to me that every companion has unique command options besides their ultimate.

Which brings me to Sammy. He's a shield-bearer who's just along for the ride. He's very genial and well-rounded, with a lot of solid utility options on his command menu.

Then there's Felix, the rogue, who just wants a paycheck. He's sneaky. Nothing personnel, kid.

And then Canine, the wild woman. She starts with zero commands but has exceptional starting stats and hidden potential in upgrading her command menu. This is kind of like a secret character I think and may not end up in the game jam version of the game.

The sixth is still a secret. It's a guest character designed by my good friend Celosia Prox. Even I don't know the first thing about this character! I will trust in Celosia to design something awesome, and this character will certainly end up in the jam version of the game.

That's all I have tonight. Thank you for watching, and don't forget to subscribe for more devlogs and to follow me on itch.io to see the finished game as soon as it's released!