# Devlog \# 2

Today I started studying C# again with the C# Player's Guide, as I expect performance to be a problem for the version of this game that I want and I'm going to pre-empt it. I'm making good progress on a good Vampire Survivors clone in Godot tutorial series by Branno. Today I did a lot of good brainstorming with my good friend Celosia Prox.

Celosia gave me her special guest companion. I won't spoil it now, but I am very excited for you to see it in the future. You can see the character when the game comes out.

We nailed down all the stats and their relationships to each other as well as all the status effects.

We started coming up with base stats for all the companions and the survivor, using a scale from 1 to 6 to track them very abstractly and relatively. I also figured out which stats the survivor and companions even need to have associated with them. I will do at least that for enemies and bosses later.

We also started coming up with lists of commands, including what is hopefully an exhaustive list of shared commands to automate companions to a small extent.

Tonight and tomorrow I will finish base stats for all the companions and figure out 4 unique commands for each of them to go with the 4 shared commands in their menus. Then, tomorrow, I will come up with branching upgrade paths on level up for all the companions and model them.

Other than that, coming up with enemies, and formalizing all the systems and mechanics I'm going to want, I will mostly be working on learning C#, Godot, and the Command pattern in depth this week. I'll be doing that and tinkering with models until I'm comfortable starting code on the project proper. If I don't start sooner, I will start coding on Sunday.

Short devlog today.