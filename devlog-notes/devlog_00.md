# Devlog \# 0

I joined this game jam two minutes before it started. The jam is two weeks (and a day) long, with my game due on September 4th.

Since the jam started two weeks and a day before the due date, I do not feel pressed to spend this first day too hastily. I'll be brainstorming plenty of ideas today and thinking of the style of game I want to make this time. Something less arcadey than my first game, I think.

I was planning to work on a roguelike until I found this jam, but I need more experience to handle a project that complex in my opinion. I have been thinking about doing a survivors-like, or bullet heaven, game soon. That might be a good fit for this jam.

The theme is "Companion." My first thought on hearing that was to make a game where the player character is defenseless but uses a competent companion or companions. Think of Lilith from Binding of Isaac. In fact, that's a great seed for a game; my joy with Lilith before she was nerfed and my frustration when it happened.

I'm going to try to make a concerted effort to follow the process outlined in Production Point by Heartbeast. I loved the book. It was what pushed me to participate in my first jam and this one. So I'll roughly block out how I want to use my time during this jam according to the book and Heartbeast's progress in a devlog series where he did the same during a game jam.

I've also been reading a book on planning software with diagrams in Mermaid, so I will attempt to do some modeling before diving into the systems I'll be working on for nearly two weeks. I want to do more planning on this jam--it's an important skill to learn. I particularly want to follow two pieces of advice I've heard. The first is an old saying: "No plan survives contact with the enemy, but planning is indispensable." The second is from Tom Francis: "It's important to make a plan and then don't stick to it." I'm paraphrasing both.

Of course, I will be using Godot 4 for this jam. I will have to decide if I'm curious enough to bite the bullet and try 3D or continue 2D. Most likely 2D, but I was doing a tutorial with Grid Maps earlier and they seem very fun, plus 3D graphics are what I'm most interested in learning. Low-poly PS1-style junk makes me very excited!

Expect regular daily devlogs with visuals and better pacing. When I say regular, I mean a consistent daily upload time, too. And I wrote an outline for this video! You're welcome.