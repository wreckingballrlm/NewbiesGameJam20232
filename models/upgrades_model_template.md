```mermaid
---
title: Upgrade Paths Template
---
flowchart TD
    Branch1 --> Branch3 & Branch4
    Branch2 --> Branch3 & Branch4

    Branch3 --> Branch5 & Branch6
    Branch5 --> Command1 & Command2
    Branch6 --> Command1 & Command2
    
    Command1 & Command2 --> UniquePassive1

    Branch4 --> Branch7 & Branch8
    Branch7 --> Command3 & Command4
    Branch8 --> Command3 & Command4
    
    Command3 & Command4 --> UniquePassive2
```