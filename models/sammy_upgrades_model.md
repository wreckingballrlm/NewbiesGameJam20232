```mermaid
---
title: Sammy's Upgrade Path
---
flowchart TD
    DefensiveBranch\nConstitution --> Defensive2\nConstitution\nKnockbackResistance & Command2\nWisdom\nCooldownReduction
    CommandBranch\nWisdom --> Defensive2\nConstitution\nKnockbackResistance & Command2\nWisdom\nCooldownReduction

    Defensive2\nConstitution\nKnockbackResistance --> Defensive3\nEnergy\nKnockbackResistance & Defensive4\nConstitution\nAttackSpeed
    Defensive3\nEnergy\nKnockbackResistance --> DefensiveCommand1\nBolsterDefenses & DefensiveCommand2\nRallyCry
    Defensive4\nConstitution\nAttackSpeed --> DefensiveCommand1\nBolsterDefenses & DefensiveCommand2\nRallyCry
    
    DefensiveCommand1\nBolsterDefenses & DefensiveCommand2\nRallyCry --> UniquePassive\nAegisOfAthena

    Command2\nWisdom\nCooldownReduction --> Command3\nWisdom\nEnergyRegen & Command4\nCooldownReduction\nAttackAreaSize
    Command3\nWisdom\nEnergyRegen --> CommandCommand1\nTacticalRetreat & CommandCommand2\nShareRations
    Command4\nCooldownReduction\nAttackAreaSize --> CommandCommand1\nTacticalRetreat & CommandCommand2\nShareRations
    
    CommandCommand1\nTacticalRetreat & CommandCommand2\nShareRations --> UniquePassive\nHolsterOfHephaestus
```