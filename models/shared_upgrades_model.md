```mermaid
---
title: Common Upgrade Path
---
flowchart TD
    RawDamageBranch\nStrength\nIntelligence --> Damage2\nPhysicalAttack\nMagicalAttack & Crit2\nCriticalChance\nCriticalDamageMultiplier
    CritBuildBranch\nIntelligence\nDexterity --> Damage2\nPhysicalAttack\nMagicalAttack & Crit2\nCriticalChance\nCriticalDamageMultiplier

    Damage2\nPhysicalAttack\nMagicalAttack --> Damage3\nStrength\nPhysicalAttack & Damage3\nIntelligence\nMagicalAttack
    
    Crit2\nCriticalChance\nCriticalDamageMultiplier --> Crit3\nIntelligence\nCriticalDamageMultiplier & Crit3\nDexterity\nCriticalChance
```