```mermaid
---
title: Brynhilda Upgrade Path
---
flowchart TD
    DefensiveBranch\nConstitution --> Defensive2\nConstitution\nKnockbackResistance & Offensive2\nStrength\nAdditionalAttack

    OffensiveBranch\nStrength --> Defensive2\nConstitution\nKnockbackResistance & Offensive2\nStrength\nAdditionalAttack

    Defensive2\nConstitution\nKnockbackResistance --> Defensive3\nKnockback\nKnockbackResistance & Defensive4\nKnockbackResistance\nCooldownReduction
    Defensive3\nKnockback\nKnockbackResistance --> DefensiveCommand1\nGuardianAngel & DefensiveCommand2\nBroadsideBash
    Defensive4\nKnockbackResistance\nCooldownReduction --> DefensiveCommand1\nGuardianAngel & DefensiveCommand2\nBroadsideBash
    DefensiveCommand1\nGuardianAngel & DefensiveCommand2\nBroadsideBash --> UniquePassive\nValkyrie

    Offensive2\nStrength\nAdditionalAttack --> Offensive3\nAdditionalAttack\nAttackAreaSize & Offensive4\nAttackAreaSize\nEffectChance
    Offensive3\nAdditionalAttack\nAttackAreaSize --> OffensiveCommand1\nCleave & OffensiveCommand2\nSpinToWin
    Offensive4\nAttackAreaSize\nEffectChance --> OffensiveCommand1\nCleave & OffensiveCommand2\nSpinToWin
    
    OffensiveCommand1\nCleave & OffensiveCommand2\nSpinToWin --> UniquePassive\nWarGoddess
```