# Actor Scene Model

```mermaid
---
title: Actor Scene Model
---
classDiagram
    Survivor "1" --|> "1" Actor: inherits from
    Companion "1" --|> "1" Actor: inherits from
    Enemy "1" --|> "1" Actor: inherits from

    Actor "1" *-- "1" CharacterBody2D: has as root node

    CharacterBody2D "1" *-- "1" Sprite2D: has
    CharacterBody2D "1" *-- "1..*" CollisionShape2D: has
    CharacterBody2D "1" *-- "1" AnimationPlayer: has
```