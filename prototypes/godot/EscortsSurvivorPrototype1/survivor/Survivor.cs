using Godot;
using System;

public partial class Survivor : Actor
{
    [Signal]
    public delegate void EnemyDetectedEventHandler(Node2D enemy);

    public override void _Ready()
    {
        base._Ready();

        controller = new PlayerController();
        AddChild(controller);
    }

    public override void _PhysicsProcess(double delta)
    {
        base._PhysicsProcess(delta);
    }

    public void OnEnemyDetectionAreaBodyEntered(Node2D body)
    {
        if ((bool)body.Call("is_in_group", "enemy"))
        {
            EmitSignal(SignalName.EnemyDetected, body);
            GD.Print("enemy detected");
        }
    }
}
