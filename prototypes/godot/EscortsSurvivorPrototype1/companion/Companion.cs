using Godot;
using System;

public partial class Companion : Actor
{
    protected Node companionMenu;
    protected bool isMouseOverlapping = false;

    public override void _Ready()
    {
        base._Ready();

        controller = new AICompanionController();
        AddChild(controller);

        companionMenu = GetNode("PopupMenu");
    }

    public override void _PhysicsProcess(double delta)
    {
        base._PhysicsProcess(delta);

        if (Input.IsActionJustPressed("select") && isMouseOverlapping)
        {
            GD.Print("clicked on companion");
            // TODO: Add CompanionMenu scene with shared commands to instance
            // companionMenu.Set("position", GlobalPosition);
            companionMenu.Set("visible", true);
        }
    }

    public void _OnClickableAreaMouseEntered()
    {
        isMouseOverlapping = true;
        // GD.Print(isMouseOverlapping);
    }

    public void _OnClickableAreaMouseExited()
    {
        isMouseOverlapping = false;
        // GD.Print(isMouseOverlapping);
    }
}
