using Godot;
using System;
[Tool]

public partial class ActorStatistics : Resource
{
    // The Six Stats
    [Export]
    public int strength { get; set; } = 1;
    [Export]
    public int intelligence { get; set; } = 1;
    [Export]
    public int constitution { get; set; } = 1;
    [Export]
    public int luck { get; set; } = 1;
    [Export]
    public int wisdom { get; set; } = 1;
    [Export]
    public int dexterity { get; set; } = 1;

    // Stats
    [Export]
    public int experienceLevel { get; set; } = 1;
    [Export]
    public int experience { get; set; } = 0;
    [Export]
    public int experienceCap { get; set; } = 15;
    [Export]
    public float experienceGainMultiplier { get; set; } = 1.0F;
    [Export]
    public int extraLives { get; set; } = 0;
    [Export]
    public int rerolls { get; set; } = 0;
    [Export]
    public float curse { get; set; } = 0.0F;

    [Export]
    public float hp { get; set; } = 50.0F;
    [Export]
    public float hpRegen { get; set; } = 0.1F;
    [Export]
    public float energyRemaining { get; set; } = 50.0F;
    [Export]
    public float energyRegen { get; set; } = 0.1F;
    [Export]
    public float might { get; set; } = 1.0F;
    [Export]
    public float defenseBonus { get; set; } = 1.0F;
    [Export]
    public float cooldownReduction { get; set; } = 0.1F;

    [Export]
    public float movementSpeed { get; set; } = 500.0F;
    [Export]
    public float physicalAttack { get; set; } = 1.0F;
    [Export]
    public float magicalAttack { get; set; } = 1.0F;
    [Export]
    public float attackSpeed { get; set; } = 1.0F;
    [Export]
    public float additionalAttack { get; set; } = 1.0F;
    [Export]
    public float attackAreaSize { get; set; } = 1.0F;

    [Export]
    public float projectileSpeed { get; set; } = 1.0F;
    [Export]
    public float projectileLifetime { get; set; } = 1.0F;

    [Export]
    public float effectChance { get; set; } = 0.1F;
    [Export]
    public float effectDuration { get; set; } = 2.0F;

    [Export]
    public float knockback { get; set; } = 1.0F;
    [Export]
    public float criticalChance { get; set; } = 0.02F;
    [Export]
    public float criticalDamageMultiplier { get; set; } = 2.0F;

    [Export]
    public float physicalDefense { get; set; } = 1.0F;
    [Export]
    public float magicalDefense { get; set; } = 1.0F;
    [Export]
    public float dodgeChance { get; set; } = 0.05F;
    [Export]
    public float knockbackResistance { get; set; } = 1.0F;
    [Export]
    public float fireResistance { get; set; } = 0.1F;
    [Export]
    public float shockResistance { get; set; } = 0.1F;
    [Export]
    public float frostResistance { get; set; } = 0.1F;
    [Export]
    public float poisonResistance { get; set; } = 0.1F;

    // Secret stat for unlocking Bob
    [Export]
    public bool isBobWatching { get; set; } = false;

}
