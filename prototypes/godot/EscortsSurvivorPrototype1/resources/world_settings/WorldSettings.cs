using Godot;
using System;
[Tool]

public partial class WorldSettings : Resource
{
    [Export]
    public float timeRemaining = 30.0F * 60.0F; // 30 minute default
}
