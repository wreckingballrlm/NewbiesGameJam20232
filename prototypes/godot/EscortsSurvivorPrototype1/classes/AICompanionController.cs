using Godot;
using System;

public partial class AICompanionController : Controller
{
    public override void _Ready()
    {
        base._Ready();
    }

    public override Vector2 GetMovement()
    {
        // TODO
        return Vector2.Zero;
    }

    public override Vector2 GetTarget()
    {
        return GetGlobalMousePosition();
    }

    public override Vector2 GetPivot()
    {
        return Vector2.Zero;
    }
}
