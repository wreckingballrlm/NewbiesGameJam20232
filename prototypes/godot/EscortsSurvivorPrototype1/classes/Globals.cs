using Godot;
using System;

// Global data

public partial class Globals : Node
{
    public float timePlayed;
    public int enemiesKilled;
    public int totalDeaths;
}
