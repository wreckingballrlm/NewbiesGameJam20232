using Godot;
using System;

// Interface where all Controller behavior is declared; inherited by Controller
interface IController
{
    // Get movementVector for Actor
    Vector2 GetMovement();
    Vector2 GetTarget();
    Vector2 GetPivot();
}
