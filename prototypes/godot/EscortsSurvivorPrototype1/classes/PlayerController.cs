using Godot;
using System;

public partial class PlayerController : Controller
{
    public override void _Ready()
    {
        base._Ready();
    }

    public override Vector2 GetMovement()
    {
        Vector2 movementVector;

        movementVector.X = Input.GetAxis("move_left", "move_right");
        movementVector.Y = Input.GetAxis("move_up", "move_down");

        return movementVector;
    }

    public override Vector2 GetTarget()
    {
        return GetGlobalMousePosition();
    }

    public override Vector2 GetPivot()
    {
        return Vector2.Zero;
    }
}
