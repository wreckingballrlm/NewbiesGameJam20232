using Godot;
using System;

public partial class Controller : Node2D, IController
{
    // Default implementations to be overrided by descendant Controllers
    // Do NOT use the base Controller class for any Actor
    public virtual Vector2 GetMovement()
    {
        return Vector2.Zero;
    }

    public virtual Vector2 GetTarget()
    {
        return Vector2.Zero;
    }

    public virtual Vector2 GetPivot()
    {
        return Vector2.Zero;
    }
}