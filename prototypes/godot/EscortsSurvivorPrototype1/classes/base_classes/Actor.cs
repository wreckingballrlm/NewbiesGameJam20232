using Godot;
using System;

public partial class Actor : CharacterBody2D
{
    [Export]
    public ActorStatistics stats;
    [Export]
    protected CSharpScript controllerScript; // Do not use base Controller

    public enum Allegiance
    {
        Friend,
        Enemy,
    }
    [Export]
    public Allegiance myAllegiance;

    // References
    protected Controller controller;
    protected Node sprite2d;

    // Useful members
    protected Vector2 movementVector;
    protected Vector2 target; // Point to look at and generally move toward & attack
    protected Vector2 pivot; // Point to orbit around

    public override void _Ready()
    {
        base._Ready();

        sprite2d = GetNode("Sprite2D");
    }

    public override void _PhysicsProcess(double delta)
    {
        base._PhysicsProcess(delta);

        Update(delta);
    }

    public virtual void InitializeController()
    {
        // Construct controller of appropriate type for game and add as child
    }

    public void Update(double delta)
    {
        Move();
        Look();
        Attack();
        Orbit();
    }

    public void Move()
    {
        movementVector = controller.GetMovement();
        Velocity = movementVector * (float)stats.Get("movementSpeed");
        // GD.Print(Velocity);

        MoveAndSlide();
    }

    public void Look()
    {
        target = controller.GetTarget();
        // GD.Print(target);
        if (target.X < GlobalPosition.X)
        {
            sprite2d.Set("flip_h", true);
        }
        else
        {
            sprite2d.Set("flip_h", false);
        }
    }

    public void Attack()
    {
        // GD.Print("attack");
    }

    public void Orbit()
    {
        pivot = controller.GetPivot();
    }
}
