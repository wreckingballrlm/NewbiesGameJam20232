using Godot;
using System;

public partial class Enemy : Actor
{
    public override void _Ready()
    {
        base._Ready();

        controller = new AICompanionController();
        AddChild(controller);
    }

    public override void _PhysicsProcess(double delta)
    {
        base._PhysicsProcess(delta);
    }
}
