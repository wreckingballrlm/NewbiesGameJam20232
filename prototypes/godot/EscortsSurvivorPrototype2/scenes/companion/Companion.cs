using Godot;
using System;

public partial class Companion : Actor
{
	private CompanionController controller;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		base._Ready();

		controller = controllerScene.Instantiate() as CompanionController;
		AddChild(controller);
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _PhysicsProcess(double delta)
	{
		base._PhysicsProcess(delta);

		Move(controller.GetMovement());
		Look(controller.GetTarget());
	}
}
