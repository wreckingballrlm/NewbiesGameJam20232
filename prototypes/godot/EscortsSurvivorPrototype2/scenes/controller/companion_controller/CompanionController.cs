using Godot;
using System;

public partial class CompanionController : Node2D, IController
{
	[Signal]
	public delegate void PulledEnemyFromQueueEventHandler(); // When manager gets this signal, it will remove enemy at front of queue

	public Node2D currentTarget;
	private CompanionTargetManager targetManager;
	private Survivor survivor;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		targetManager = GetTree().GetFirstNodeInGroup("companion_target_manager") as CompanionTargetManager;

		survivor = GetTree().GetFirstNodeInGroup("survivor") as Survivor;
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		base._Process(delta);

		survivor = GetTree().GetFirstNodeInGroup("survivor") as Survivor;
	}

	public Vector2 GetMovement()
	{
		if (currentTarget != null)
		{
			return GlobalPosition.DirectionTo(currentTarget.GlobalPosition);
		}
		else if (survivor != null)
		{
			return GlobalPosition.DirectionTo(survivor.GlobalPosition);
		}
		else
		{
			return Vector2.Zero;
		}
	}

	public Vector2 GetTarget() // seems to work okay. need to implement nulling enemy reference when companion kills enemy
	{
		if (currentTarget == null && targetManager.enemies.Count > 0)
		{
			currentTarget = targetManager.enemies[0];
			EmitSignal(SignalName.PulledEnemyFromQueue);
			return GlobalPosition.DirectionTo(currentTarget.GlobalPosition);
		}
		else if (currentTarget != null)
		{
			return GlobalPosition.DirectionTo(currentTarget.GlobalPosition);
		}
		else if (survivor != null) // currentTarget is null or enemies is empty; either way, it should be safe to set currentTarget to null
		{
			currentTarget = null;
			return GlobalPosition.DirectionTo(survivor.GlobalPosition);
		}
		else
		{
			currentTarget = null;
			return Vector2.Zero;
		}
	}
}
