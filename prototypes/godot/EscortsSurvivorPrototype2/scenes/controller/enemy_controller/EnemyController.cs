using Godot;
using System;

public partial class EnemyController : Node2D, IController
{
	Survivor survivor;

	public override void _Ready()
	{
		base._Ready();

		survivor = GetTree().GetFirstNodeInGroup("survivor") as Survivor;
	}

	public override void _Process(double delta)
	{
		base._Process(delta);

		survivor = GetTree().GetFirstNodeInGroup("survivor") as Survivor;
	}

	public Vector2 GetMovement()
	{
		if (survivor != null)
		{
			return GlobalPosition.DirectionTo(survivor.GlobalPosition);
		}
		else
		{
			return Vector2.Zero;
		}
	}

	public Vector2 GetTarget()
	{
		return Vector2.Zero;
	}
}
