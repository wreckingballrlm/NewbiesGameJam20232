using Godot;
using System;

public partial class PlayerController : Node2D, IController
{
	public Vector2 GetMovement()
	{
		var movementVector = Vector2.Zero;

		movementVector.X = Input.GetAxis("move_left", "move_right");
		movementVector.Y = Input.GetAxis("move_up", "move_down");

		return movementVector;
	}

	public Vector2 GetTarget()
	{
		return GetGlobalMousePosition();
	}
}
