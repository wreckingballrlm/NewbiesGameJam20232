using Godot;
using System;

public partial class HealthComponent : Node
{
	[Signal]
	public delegate void DiedEventHandler();
	
	float currentHealth;
	Actor parent;


	public override void _Ready()
	{
		parent = GetParent() as Actor;
		if (parent != null)
		{
			currentHealth = parent.stats.maxHP;
			GD.Print(currentHealth);
		}
	}

	public void Damage(float damage)
	{
		GD.Print($"took {damage} damage");
		currentHealth = Mathf.Max(currentHealth - damage, 0);
		Callable.From(() => CheckDeath()).CallDeferred(); // Making callables from methods this way is very useful
	}

	public void CheckDeath()
	{
		if (currentHealth == 0)
		{
			EmitSignal(SignalName.Died);
			Owner.CallDeferred("queue_free");
		}
	}
}
