using Godot;
using System;

public partial class HitboxComponent : Area2D
{
	public float damage; // Insert damage from damage formula calc'd in parent in Ready
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		damage = 5.0F;
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}
}
