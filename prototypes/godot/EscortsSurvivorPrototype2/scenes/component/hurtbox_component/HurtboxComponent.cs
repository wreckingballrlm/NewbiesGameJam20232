using Godot;
using System;

public partial class HurtboxComponent : Area2D
{
	[Export]
	HealthComponent healthComponent;

    public override void _Ready()
    {
        AreaEntered += (Area2D area) => OnAreaEntered(area);
    }

	public void OnAreaEntered(Area2D area)
	{
		if (area.IsClass("HitboxComponent")) // What does this do?
		{
			return;
		}

		if (healthComponent == null)
		{
			return;
		}

		HitboxComponent hitbox = area as HitboxComponent;
		if (hitbox != null)
		{
			healthComponent.Damage(hitbox.damage);
		}
	}
}
