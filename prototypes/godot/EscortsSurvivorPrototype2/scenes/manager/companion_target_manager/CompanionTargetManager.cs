using Godot;
using System;

public partial class CompanionTargetManager : Node
{
	public System.Collections.Generic.List<Node2D> enemies;
	Survivor survivor;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		enemies = new System.Collections.Generic.List<Node2D>();
		survivor = GetTree().GetFirstNodeInGroup("survivor") as Survivor;

		survivor.EnemyDetected += (Node2D enemy) => AddEnemyToQueue(enemy);
		survivor.EnemyLeftRange += (Node2D enemy) => RemoveEnemyFromQueue(enemy);
	}

	private void AddEnemyToQueue(Node2D enemy)
	{
		GD.Print($"Added {enemy} to queue");
		enemies.Add(enemy);
	}

	private void RemoveEnemyFromQueue(Node2D enemy)
	{
		GD.Print($"Removed {enemy} from queue");
		enemies.Remove(enemy);
	}
}
