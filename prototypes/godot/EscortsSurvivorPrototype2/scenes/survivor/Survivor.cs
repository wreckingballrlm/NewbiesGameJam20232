using Godot;
using System;

public partial class Survivor : Actor
{
	[Signal]
	public delegate void EnemyDetectedEventHandler(Node2D enemy);
	[Signal]
	public delegate void EnemyLeftRangeEventHandler(Node2D enemy);

	private PlayerController controller;
	private Area2D enemyDetectionZone;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		base._Ready();

		controller = controllerScene.Instantiate() as PlayerController;
		AddChild(controller);
		enemyDetectionZone = GetNode("EnemyDetectionZone") as Area2D;

		enemyDetectionZone.BodyEntered += (Node2D body) => OnEnemyDetected(body);
		enemyDetectionZone.BodyExited += (Node2D body) => OnEnemyLeftRange(body);
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _PhysicsProcess(double delta)
	{
		base._Process(delta);

		Move(controller.GetMovement());
		Look(controller.GetTarget());
	}

	public void OnEnemyDetected(Node2D body)
	{
		GD.Print("enemy detected");
		EmitSignal(SignalName.EnemyDetected, body);
	}

	public void OnEnemyLeftRange(Node2D body)
	{
		GD.Print("enemy left range");
		EmitSignal(SignalName.EnemyLeftRange, body);
	}
}
