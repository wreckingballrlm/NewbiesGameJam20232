using Godot;
using System;

public partial class Actor : CharacterBody2D
{
	// Things all actors need wired up go in here
	[Export]
	public ActorStatistics stats;
	[Export]
	protected PackedScene controllerScene;
	[Export]
	public Allegiance allegiance;

	protected Sprite2D sprite;

	public enum Allegiance {
		Friend,
		Enemy,
	};

	public override void _Ready()
	{
		sprite = GetNode("Sprite2D") as Sprite2D;
	}

	public void Move(Vector2 movementVector)
	{
		Velocity = movementVector * stats.movementSpeed;

		MoveAndSlide();
	}

	public void Look(Vector2 target)
	{
		if (target.X < GlobalPosition.X)
		{
			sprite.SetDeferred("flip_h", true);
		}
		else
		{
			sprite.SetDeferred("flip_h", false);
		}
	}
}
