using Godot;
using System;

public partial class ActorStatistics : Resource
{
    // Stats for all actors go in here

    // Experience
    [Export]
    public int experienceLevel = 1;
    [Export]
    public float experienceGainMultiplier = 1.0F;

    // Core Stats
    [Export]
    public int strength = 1;
    [Export]
    public int intelligence = 1;
    [Export]
    public int constitution = 1;
    [Export]
    public int luck = 1;
    [Export]
    public int wisdom = 1;
    [Export]
    public int dexterity = 1;
    [Export]
    public float movementSpeed = 500.0F;
    [Export]
    public float maxHP = 50.0F;

    // Efficiency Stats
    [Export]
    public float hpRegen = 0.0F;
    [Export]
    public int energy = 1;
    [Export]
    public float energyRegen = 0.1F;
    [Export]
    public float cooldownReduction = 0.0F;

    // Meta Stats
    [Export]
    public int might = 0;
    [Export]
    public int defenseBonus = 0;
    [Export]
    public int extraLives = 0;
    [Export]
    public int rerolls = 0;
    [Export]
    public int curse = 0;

    // Attack Stats
    [Export]
    public float attackAreaSize = 1.0F;
    [Export]
    public float projectileSpeed = 1.0F;
    [Export]
    public float projectileLifetime = 1.0F;
    [Export]
    public float physicalAttack = 1.0F;
    [Export]
    public float knockback = 1.0F;
    [Export]
    public float magicalAttack = 1.0F;
    [Export]
    public float attackSpeed = 1.0F;
    [Export]
    public float additionalAttack = 1.0F;

    // Random Stats
    [Export]
    public float criticalChance = 0.02F;
    [Export]
    public float criticalDamageMultiplier = 2.0F;
    [Export]
    public float effectChance = 0.1F;
    [Export]
    public float effectDuration = 2.0F;

    // Defense Stats
    [Export]
    public float physicalDefense = 1.0F;
    [Export]
    public float knockbackResistance = 1.0F;
    [Export]
    public float magicalDefense = 1.0F;
    [Export]
    public float fireResistance = 0.1F;
    [Export]
    public float shockResistance = 0.1F;
    [Export]
    public float frostResistance = 0.1F;
    [Export]
    public float poisonResistance = 0.1F;
    [Export]
    public float dodgeChance = 0.0F;

    // Secret Stats
    [Export]
    public bool isBobWatching = false;

}
