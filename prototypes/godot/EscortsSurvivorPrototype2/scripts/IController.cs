using Godot;
using System;

interface IController
{
	Vector2 GetMovement();
	Vector2 GetTarget();
}
