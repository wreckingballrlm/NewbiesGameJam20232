# Brainstorm - Day 2

I'm considering whether signals would be a good fit for effecting the Command pattern in my Controller class. They aren't really meant for doing something constantly, but perhaps Actors could just act on the last signal/Command they received, including a signal to stop in the case of a PlayerController ceasing input or an Enemy that gives commands dying.

I also need to consider the role of state machines in the game very early. They are very common in game development, and I have already found good resources for making state machines in Godot.

I know I will want to use C# scripts in this game, though I don't know much C# right now. I can limit the amount of on-screen entities before I have the knowledge to replace some functionality in C#.

I want controlling companions to be like Halls of Torment and allow toggling auto-attack and auto-aim. I thought about how companions can prioritize which enemies to move to and attack before orders and automation; I decided the survivor can have a sizable circle Area2D which adds enemies that enter it into a queue the companions will use to prioritize who to attack.

Also, I think since experience is money and these games usually have metaprogression (jam version may or may not,) the experience gained should be totaled every run and added straight to a pool to spend on metaprogression.

Tonight I want to brainstorm all the stats and status effects I can. I also want to begin brainstorming derived stats and the formulas to derive them. Finally, I'd like to brainstorm base stats (abstractly) for all the companions as well as which stats they can upgrade and how.

## Statistics

I will start with the list from yesterday's brainstorming.

- Attack speed
- Movement speed
- Critical chance
- Critical damage
- Projectile amount
- Attack area size
- Projectile area size
- Projectile speed
- Projectile lifetime
- Effect chance
- Strength
- Dexterity
- Intelligence

Then I will continue from there.

- Knockback
- HP
- Energy
- Level
- Physical defense
- Magical defense
- Block strength (see Halls of Torment)
- Health regeneration
- Range
- Additional attack (see Halls of Torment)
- Duration (ultimates/commands/summons?)
- Experience gain multiplier
- Cooldown reduction
- Luck
- Curse (see Vampire Survivors)
- Rerolls
- Extra lives

## Calling it early

I didn't do nearly as much as I wanted, but my sleep has been very strange. I'm going to bed and I'll pick up on day 3. Everything will be fine.