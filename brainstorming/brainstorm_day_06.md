# Brainstorm - Day 6

I didn't finish upgrade paths yesterday or enumerate the concepts the game needs. Instead, I spent a lot of time tinkering with a small prototype to try and get interface code working the way I wanted it to. I did get it working, and an actor's controller can be set from an export field, though the constructor for it is hard-coded to certain kinds of controllers, so that needs more work after the game jam.

Now that I've got that working, I need to stop getting ahead of myself and focus on the tasks I set out for myself.

## Today's To-Do

- Work on tutorials
- Finish upgrade paths for all companions
- Model all upgrade paths
- Model actor behavior every frame
- Brainstorm pickups

And think about how data-driven design fits in to this project. I learned how to put a bunch of export variables into a resource instead from some other tutorial. Can I do that here for Actor? Can I employ data-driven design for the companions and their upgrades as well?

Brainstorm and start modeling a manager that sits in the World and serves the shared information Actors (especially companions) need while making sure none of them disrupt it. Think of it like a switch that handles the signals for the survivor detecting enemies and distributes the tasks of attacking them to companions based on their automation settings, at its most basic. Consider how this would behave differently if a player took over a companion. Basically, don't use controllers for this unless ALL controllers use it correctly. This system will eventually help track bosses and their companions, too, but that's not going in the game jam version so no worries for now.