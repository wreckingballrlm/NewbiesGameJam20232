# Brainstorm - Day 7

I've missed a few days and my plan is falling apart. I'm not worried, however. I already made some progress on the prototype. I need to finish grouping all the companion stats, model all the upgrade paths, come up with formulas for the stats and pickups, define all the parts each scene will need, and model actor behavior (after learning about behavior trees.) Those are all the design tasks left besides tweaking things while working on the prototype. That is very manageable.

I also want to finish the Survivors clone tutorial I was doing. I may be able to pick up a Survivors clone course on Udemy tomorrow, and if so I will start that but it won't be a priority.

I will split all this work up between today and tomorrow, and I will stop using bad sleep as an excuse. It sucks, but even if I don't sleep, I will still dev. I can sneed.

I will start working hands-on with the prototype again, with no other tasks on my plate, on Tuesday. That gives me just under a week, but I already made some good progress on the prototype last week, so I'm not worried. Hell, if I don't get as polished a product as I want submitted in time, I'm not worried about that. I'll turn in what I can and then work on this one some more later. That's no excuse to dawdle, though. I've missed a few steps but it's time to get back on track.

## The Companions

Here they are. Finish grouping their stats and work on modeling their upgrade paths, plus the one path that upgrades all of them, today.

As a note: for tracking their prerequisites, I will use a stat called upgradesAcquired that simply counts how many times they've been upgraded. They can also either have extra flags for traversing different paths/branches or a dictionary of upgrades that is checked to see which ones they have access to. We'll see.

### Brynhilda

### Upgrade Path 1

### Branch 1 - Defensive

Stats that go up and their relationship:

- Constitution
- Knockback
- Knockback Resistance
- Knockback, Cooldown Reduction

Commands that get upgraded:

- Guardian Angel
- Broadside Bash

Unique Passive: Valkyrie - Permanently buff all companions' defense stats by 30% and a small permanent movement speed buff (5%?)

### Branch 2 - Offensive

Stats that go up and their relationship:

- Strength
- Attack Area Size
- Additional Attack
- Additional Attack, Effect Chance

Commands that get upgraded:

- Cleave
- Spin to Win

Unique Passive: War Goddess - Permanently buff all companions' attack area size by 20% and increase damage by 10%

### Sammy

### Branch 1 - Defensive

Stats that go up and their relationship:

- Constitution
- Energy
- Knockback Resistance
- Constitution, Attack Speed

Commands that get upgraded:

- Bolster Defenses: Temporary buff, enhanced all defense stats 1 minute buff and 1 minute cooldown
- Rally Cry: Temporary buff, enhanced attack and attack speed 1 minute buff and 1 minute cooldown

Unique Passive: Aegis of Athena - Receive a charge of invincibility to the first hit received every five minutes. Holds up to two charges.

### Branch 2 - Commands

Stats that go up and their relationship:

- Wisdom
- Energy Regen
- Cooldown Reduction
- Cooldown Reduction, Attack Area Size

Commands that get upgraded:

- Tactical Retreat: Temporary buff, 150 to 200 percent movement speed 15 second buff 30-60 second cooldown
- Share Rations: Small heal, 10 to 30 percent

Unique Passive: Holster of Hephaestus - Reduces all of Sammy's cooldowns by 50%.

### Diego

### Branch 1 - Mitigate Low Energy

Stats that go up and their relationship:

- Dexterity
- Cooldown Reduction
- Attack Speed
- Cooldown Reduction, Energy

Commands that get upgraded:

- Fastball Special: Picks up survivor for a limited time, offering invincibility and movement speed buff. Activating again during the duration throws survivor at targeted location
- Looking for Trouble: Diego blindly attacks random enemies for a few seconds with improved attack damage and attack speed

Unique Passive: Riled Up - When Diego uses Looking For Trouble, all companion's now receive the bonus. The bonus is increased by 10%.

### Branch 2 - MOAR OFFENSE

Stats that go up and their relationship:

- Strength
- Attack Speed
- Additional Attack
- Attack Speed, Critical Damage Multiplier

Commands that get upgraded:

- Molotov Cocktails: Throw molotovs creating small AoE damage zones where they land with chance to inflict burn.
- Drunken Stupor: Diego trips and falls, causing a large shockwave in place with high knockback and stun chance

Unique Passive: Buy A Round - Permanently buff all companions' attack speed by 20%

### Felix

### Branch 1 - Projectiles

Stats that go up and their relationship:

- Intelligence
- Projectile Lifetime
- Projectile Speed
- Projectile Lifetime, Attack Area Size

Commands that get upgraded:

- Volley of Bolts: Rapidly fires crossbow bolts in a steady line facing one direction while moving to a targeted location.
- Poison Coating: Temporary buff, adds poison effect to all companions' attacks

Unique Passive: Viper's Tongue - Permanent poison coating for everybody

### Branch 2 - Crits, Crits, Crits

Stats that go up and their relationship:

- Dexterity
- Attack Speed
- Critical Chance
- Attack Speed, Knockback

Commands that get upgraded:

- Teleports Behind You: Teleports to target location and gains 100% crit chance for a few seconds
- Thief's Eye: Increases experience gain passively while in the party

Unique Passive: Rogueish Intuition - Double effect of Thief's Eye, Thief's Eye now gives increased crit chance for all companions

### Bob

### Branch 1 - Spammer

Stats that go up and their relationship:

- Wisdom
- Cooldown Reduction
- Magical Attack
- Cooldown Reduction, Energy Regen

Commands that get upgraded:

- Dragon Breath: Uses a random attack composed of a shape (small circle, beam, cone) and an element (fire, lightning, frost, poison.)
- Drink and be Merry: Summons a pack of drunken kobolds who drink from a barrel of booze at target location before scattering and attacking random enemies. Kobolds have a 5% chance to attack each other, causing a cartoon dustcloud quarrel that makes an AoE of damage

Unique Passive: Increase damage and attack area size of Dragon's Breath by 50%. Uses two attacks at once.

### Branch 2 - Critter

Stats that go up and their relationship:

- Intelligence
- Critical Damage Multiplier
- Effect Chance
- Critical Damage Multiplier, Critical Chance

Commands that get upgraded:

- Magic Missile: Shoot a random ranged attack that is either an explosive fireball, chain lightning, precise frost bullets, or a AoE/spreading poison effect
- Then I Started Blasting: Uses random unique commands for a few seconds without expending more energy

Unique Passive: Kobold's Mischief - Doubles everything related to kobold summon, halves cooldown