# Brainstorm - Day 4

Last night I was thinking about interfaces in OOP and finally got around to reading up on them just a bit in bed. The concept clicked for me, and I get why they're useful, at least for my use case on this game.

I'll paste what I sent Celosia to make it stick in my brain in here:

"I am writing a message because I just read up on something that finally clicked and I want it to stay that way. Interfaces in object-oriented programming are like classes that are meant to be implemented by any number of other classes and come with methods but don't come with definitions for those methods.

What's the use of this with the command pattern and my game? That's what I've been wondering and now I get it

Let's say the actor class that the survivor, companions, and enemies are all derived from wants to call a method called GetMovement from its controller. GetMovement is an interface. So there exists somewhere in the code an interface with a function called GetMovement that doesn't do anything. The beauty comes when the controller class's subclasses implement that interface.

Actor calls GetMovement on its Controller without caring what kind of controller it is. All Controllers implement GetMovement in their own way and give Actor what it expects, just in unique ways.

So PlayerController's version of GetMovement would actually get input from the player. An AIController would  GetMovement based on its AI decision-making. But the Actor doesnt care, it just knows that its controller has a method called GetMovement that will give it a specific kind of data

So I guess in Command pattern I'll want every action in the game that actors can take to be an Interface, and then implement that interface in every version of the Controller class. I think Actors should have an export variable slot for a Controller to be attached to them
That one piece of info was so important, I'm so glad it clicked conceptually. Still a long way to go though
"

I also realized a lot of survivors games basically use the shareware model; extensive demos that let you transfer your save data over to the full game. I want to do that if this game reaches full production.

Last night we also decided the theme of the game should lean into D&D, Dead Rising, and '90s nostalgia. Eclectic. I like it. I want the exp. pickups to be electrum, bronze, silver, gold, and platinum. I also wrote this on stats and how their actual numbers work:

"**BASE STAT BASELINES**
Abstract stats are used to derive factors for bonuses added to other stats except in special cases noted. Whole numbers can be stored as integers and anything with a decimal should be a floating point number.

Strength: 1 (abstract)

Intelligence: 1 (abstract)

Constitution: 1 (abstract)

Luck: 1 (abstract)

Wisdom: 1 (abstract)

Dexterity: 1 (abstract)

Experience level: 1 (abstract - used as a multiplier for all other stats)

Energy: 1 (abstract)

Might: 1 (abstract)

Defense Bonus: 1 (abstract)

Attack Area Size: 1.0 (multiplier for size of attack pattern collision shape)

Projectile Speed: 1.0 (multiplier for movement speed of projectile instances)

Extra Lives: 1 (abstract - metacurrency)

Re-rolls: 1 (abstract - metacurrency)

Curse: 1 (abstract - used as a multiplier for enemy stats)

Physical Attack: 1.0 (multiplier for physical attack damage)

Knockback: 1.0 (multiplier for distance enemies are pushed in pixels when attacked)

Magical Attack: 1.0 (multiplier for magical attack damage)

Projectile Lifetime: 1.0 (in seconds)

HP: 50.0 (abstract, number is placeholder)

Physical Defense: 1.0 (multiplier for physical damage subtracted before being applied to health)

HP Regen: 0.1 (multiplier for health restored each second, number is placeholder)

Knockback Resistance: 1.0 (multiplier for distance subtracted from knockback before being applied to physics body)

Experience Gain Multiplier: 1.0 (multiplier for experience gained from experience pickups)

Critical Chance: 0.02 (number to roll under between 0 and 1 to determine if attack causes critical damage, number is placeholder)

Critical Damage Multiplier: 2.0 (multiplier for attack damage before being applied to target)

Magical Defense: 1.0 (multiplier for damage subtracted from magical attacks before being applied to HP)

Cooldown Reduction: 0.1 (seconds subtracted from cooldown timer after using cooldown abilities. number is placeholder)

Fire Resistance (and all other elemental resistances): 0.1 (number to roll under between 0 and 1 to determine if elemental effect is applied by elemental attack, number is placeholder)

Attack Speed: 1.0 (number of seconds between attacks, inverse relationship with being "raised")

Movement Speed: 500.0 (number of pixels can move across screen every second, number is placeholder)

Additional Attack: 1.0 (multiplier for number of times damage is applied by each attack)

Effect Chance: 0.1 (number rolled under between 0 and 1 to determine if elemental effect is applied by elemental attack, number is placeholder)

Effect Duration: 2.0 (number of seconds effect applied lasts, number is placeholder)

Energy Regen 0.1 (amount of energy bar restored each second, number is placeholder)

Dodge Chance: 0.05 (number rolled under between 0 and 1 to determine if damage from an attack and chance of effect being applied is nullified before being applied, number is placeholder)"

Secret Bob Stat: After obtaining a million gold on your savefile, any time you look at the stats menu there is a 25% chance to unlock Bob with a special notification.

## Commands

Finish coming up with unique commands for all the companions today. I decided not to include Canine in the jam unless I end up with a lot of time and confidence at the end of the jam.

### Brynhilda

- Cleave: Temporary buff, enhanced attack area size (and attack speed?) for self
- Guardian Angel: Temporary buff, enhanced defense stats for survivor and enhanced knockback resistance for survivor and companions
- Broadside Bash: Short 180 degree Cone AoE attack, with significant knockback and stun effect
- Spin to Win: Spins her axe in a circle while orbiting the survivor. High damage and knockback

### Sammy

- Tactical Retreat: Temporary buff, 150 to 200 percent movement speed 15 second buff 30-60 second cooldown
- Bolster Defenses: Temporary buff, enhanced all defense stats 1 minute buff and 1 minute cooldown
- Rally Cry: Temporary buff, enhanced attack and attack speed 1 minute buff and 1 minute cooldown
- Share Rations: Small heal, 10 to 30 percent

### Diego

- Fastball Special: Picks up survivor for a limited time, offering invincibility and movement speed buff. Activating again during the duration throws survivor at targeted location
- Molotov Cocktails: Throw molotovs creating small AoE damage zones where they land with chance to inflict burn.
- Drunken Stupor: Diego trips and falls, causing a large shockwave in place with high knockback and stun chance
- Looking for Trouble: Diego blindly attacks random enemies for a few seconds with improved attack damage and attack speed

### Felix

- Teleports Behind You: Teleports to target location and gains 100% crit chance for a few seconds
- Volley of Bolts: Rapidly fires crossbow bolts in a steady line facing one direction while moving to a targeted location.
- Poison Coating: Temporary buff, adds poison effect to all companions' attacks
- Thief's Eye: Increases experience gain passively while in the party

### Bob

- Dragon Breath: Uses a random attack composed of a shape (small circle, beam, cone) and an element (fire, lightning, frost, poison.)
- Drink and be Merry: Summons a pack of drunken kobolds who drink from a barrel of booze at target location before scattering and attacking random enemies. Kobolds have a 5% chance to attack each other, causing a cartoon dustcloud quarrel that makes an AoE of damage
- Magic Missile: Shoot a random ranged attack that is either an explosive fireball, chain lightning, precise frost bullets, or a AoE/spreading poison effect
- Then I Started Blasting: Uses random unique commands for a few seconds without expending more energy

## Upgrade Paths

Brainstorm complete upgrade paths for every companion today, then start modeling them with Mermaid.

Each upgrade path gets one of two unique passive bonuses at the end. See model sketch for how these are structured. Three upgrade paths later, but only one for the game jam prototype. Decided not to lower stats at all and to only give bonuses to players. (KISS)

### Brynhilda

### Upgrade Path 1

### Branch 1 - Defensive

Stats that go up and their relationship:

- Constitution
- Knockback
- Knockback Resistance
- Knockback Resistance, Cooldown Reduction

Commands that get upgraded:

- Guardian Angel
- Broadside Bash

Unique Passive: Valkyrie - Permanently buff all companions' defense stats by 30% and a small permanent movement speed buff (5%?)

### Branch 2 - Offensive

Stats that go up and their relationship:

- Strength
- Attack Area Size
- Additional Attack
- Additional Attack, Effect Chance

Commands that get upgraded:

- Cleave
- Spin to Win

Unique Passive: War Goddess - Permanently buff all companions' attack area size by 20% and increase damage by 10%

### Sammy

### Branch 1 - Defense

Unique Passive: Aegis of Athena - Receive a charge of invincibility to the first hit received every five minutes. Holds up to two charges.

### Branch 2 - Commands

Unique Passive: Holster of Hephaestus - Reduces all of Sammy's cooldowns by 50%.

### Diego

### Branch 1 - Mitigate Low Energy

Unique Passive: Riled Up - When Diego uses Looking For Trouble, all companion's now receive the bonus. The bonus is increased by 10%.

### Branch 2 - MOAR OFFENSE

Unique Passive: Buy A Round - Permanently buff all companions' attack speed by 20%

### Felix

### Branch 1

Unique Passive: Viper's Tongue - Permanent poison coating for everybody

### Branch 2

Unique Passive: Rogueish Intuition - Double effect of Thief's Eye, Thief's Eye now gives increased crit chance for all companions

### Bob

### Branch 1

Unique Passive: Increase damage and attack area size of Dragon's Breath by 50%. Uses two attacks at once.

### Branch 2

Unique Passive: Kobold's Mischief - Doubles everything related to kobold summon, halves cooldown

## Current Thoughts on Command Pattern with Interfaces

Right now I'm thinking that a small handful of classes will be doing a lot of work in this game.

The Actor class will hold all statistics an actor can have and interfaces for any action another entity will ask the actor to do. The Actor class will have a tidy list of calls in the process functions which defer to the Controller attached to the Actor for what to do. Those functions will actually be interfaces implemented by each type of Controller.

The Controller class will hold all the interfaces Actors expect to operate with from Controller. Controller will also be responsible for signaling certain things up to World, like when the player pauses the game and things like that. Those can be PlayerController exclusive functionality.

## Last Thoughts for the Day

I think I'll add more nodes to the upgrade paths but keep the branching the same way. Upgrade every companion up to six times I guess. I wanted to start modeling today, but instead I'll just try to get the branches roughly planned before bed and model tomorrow. It's okay to take a day off (or mostly off) once in a while. I took last Wednesday off, too, so maybe that says something about my limits.

Maybe I should keep the scale down and only let companions be upgraded three times each. They have fewer stats they're good at than I considered. Or perhaps every other upgrade adds unique bonuses to their commands?