# Brainstorm - Day 5

Today I need to finish grouping stats for companions as well as setting their relative stat strengths compared to each other. I've only finished Brynhilda.

I also wish to start modeling their upgrade paths today.

Besides my C# and pattern studies, I also wish to start enumerating certain things that need to be in the game. For example, pickups and menus required. This includes code concepts, too. I want to make a list of all the different kinds of functionality Actors and Controllers can have, as well as unique functionality their descendants can have. Here's one I thought of last night; an Allegiance enumerator in Actor that determines if the Actor is on the survivor side or the enemy side. I feel that's important info for designing algorithms for finding targets.

I also think I need to have some kind of class that's meant to sit in the World tree and track information shared by groups of Actors. For example, an object that stores the queue companions use to target enemies that are close to the survivor. This shared queue (or whatever data structure I end up using) is used to divvy the targets up among them and reduce redundant information in the game.

I think I figured out the interface thing. It works if I put the interface and its derivatives in the Actor class and set the appropriate kind of controller in the descendant scenes' scripts. This means every actor will hold every kind of interface though, and that seems like bloat to me. I was trying to use an outside script before but couldn't figure out how to get at the method correctly. I'll either figure that out later, or perhaps use Using statements to copy in the appropriate controller to the appropriate place at the appropraite time. Figuring out how to make the controller dynamic based on game settings/modes will also be a challenge later. For now, it's fine, though. For the game jam the most I want to accomplish as far as dynamic controllers is being able to set it from an export field in the inspector. Is that possible (easily)?

## Upgrade Paths

I'll copy what we had yesterday and finish them here. For now, the upgrade paths just allow the companions to get better at what they're already good at and mitigate one stat they're poor at per branch. Future upgrade paths will allow boosting their average stats instead and I'll have to make a mega model with all the paths and how you get locked out over time. For the game jam, this will be plenty.

Also, make an upgrade path to upgrade all companions passively. Just like the companions, it will be just one path for the jam but be expanded to at least 3 in the future.

### Brynhilda

### Upgrade Path 1

### Branch 1 - Defensive

Stats that go up and their relationship:

- Constitution
- Knockback
- Knockback Resistance
- Knockback, Cooldown Reduction

Commands that get upgraded:

- Guardian Angel
- Broadside Bash

Unique Passive: Valkyrie - Permanently buff all companions' defense stats by 30% and a small permanent movement speed buff (5%?)

### Branch 2 - Offensive

Stats that go up and their relationship:

- Strength
- Attack Area Size
- Additional Attack
- Additional Attack, Effect Chance

Commands that get upgraded:

- Cleave
- Spin to Win

Unique Passive: War Goddess - Permanently buff all companions' attack area size by 20% and increase damage by 10%

### Sammy

### Branch 1 - Defense

Stats that go up and their relationship:

- Constitution
- Energy
- Knockback Resistance
- Constitution, Attack Speed

Commands that get upgraded:

- Bolster Defenses: Temporary buff, enhanced all defense stats 1 minute buff and 1 minute cooldown
- Rally Cry: Temporary buff, enhanced attack and attack speed 1 minute buff and 1 minute cooldown

Unique Passive: Aegis of Athena - Receive a charge of invincibility to the first hit received every five minutes. Holds up to two charges.

### Branch 2 - Commands

Stats that go up and their relationship:

- Wisdom
- Energy Regen
- Cooldown Reduction
- Cooldown Reduction, Attack Area Size

Commands that get upgraded:

- Bolster Defenses: Temporary buff, enhanced all defense stats 1 minute buff and 1 minute cooldown
- Rally Cry: Temporary buff, enhanced attack and attack speed 1 minute buff and 1 minute cooldown

Unique Passive: Holster of Hephaestus - Reduces all of Sammy's cooldowns by 50%.

### Diego

### Branch 1 - Mitigate Low Energy

Stats that go up and their relationship:

Commands that get upgraded:

- Fastball Special: Picks up survivor for a limited time, offering invincibility and movement speed buff. Activating again during the duration throws survivor at targeted location
- Looking for Trouble: Diego blindly attacks random enemies for a few seconds with improved attack damage and attack speed

Unique Passive: Riled Up - When Diego uses Looking For Trouble, all companion's now receive the bonus. The bonus is increased by 10%.

### Branch 2 - MOAR OFFENSE

Stats that go up and their relationship:

Commands that get upgraded:

- Molotov Cocktails: Throw molotovs creating small AoE damage zones where they land with chance to inflict burn.
- Drunken Stupor: Diego trips and falls, causing a large shockwave in place with high knockback and stun chance

Unique Passive: Buy A Round - Permanently buff all companions' attack speed by 20%

### Felix

Stats that go up and their relationship:

Commands that get upgraded:

### Branch 1

Unique Passive: Viper's Tongue - Permanent poison coating for everybody

### Branch 2

Unique Passive: Rogueish Intuition - Double effect of Thief's Eye, Thief's Eye now gives increased crit chance for all companions

### Bob

Stats that go up and their relationship:

Commands that get upgraded:

### Branch 1

Unique Passive: Increase damage and attack area size of Dragon's Breath by 50%. Uses two attacks at once.

### Branch 2

Unique Passive: Kobold's Mischief - Doubles everything related to kobold summon, halves cooldown