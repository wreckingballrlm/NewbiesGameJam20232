# Brainstorm - Day 1

## Theme

Companion

## Optional Challenge

Optionally include all three of the following mechanics.

1. Sound Control
2. Collectibles
3. Secret Area

## Escorts Survivor

I got an idea very quickly on hearing the theme which I think I am going to run with; Escort Survivors (or Escorts Survivor, a play on words since the title makes me think of Dead Rising and therefore I will be copying the UI from that game.)

You play as a defenseless but nimble-footed survivor and start with a loyal companion who is invincible and capable of repelling the small hordes at the beginning of the game without much input. You can move around with WASD and click on the companion to bring up a small command menu. Each command costs some energy which the companion recharges over time.

As your companion slays the horde, each enemy drops money which functions as experience. When you level up, you choose one of three upgrades pulled from a pool for your companion(s.) By exploring or defeating mini-bosses you can find burner phones that let you choose a new companion, up to a total of four companions per run.

Upgrades correspond to two or three branching paths per companion trait. Some upgrades apply to all companions passively, some apply to specific companions passively, and some add new commands to specific companions. You never upgrade the survivor in any way.

## Scheduling

I'm going to try to have a plan. Tonight I will block out time on a day-by-day basis for the duration of the jam. Then I will start brainstorming companions, statistics, upgradable traits, status effects, commands. Tomorrow I will start making models for game systems in Mermaid.

- Sunday 8/20: Game jam starts. Begin brainstorming. Plan.
- Monday 8/21 through Wednesday 8/23: Brainstorm. Model game logic. Paper prototype. Organize.
  
## Statistics

The game will have many statistics for each actor in the game. The survivor is the flattest, with most stats being irrelevant and him not receiving any upgrades during the game. Companions, on the other hand, have unique base stat compositions and upgrading them when leveling up allows you to choose one of three or four branches for enhancing one or two stats while diminishing another, specializing your companions to your liking.

Here are some stats I definitely want for the game:

- Attack speed
- Movement speed
- Critical chance
- Critical damage
- Projectile amount
- Attack area size
- Projectile area size
- Projectile speed
- Projectile lifetime
- Effect chance
  
And some for calculating specific kinds of damage:

- Strength (physical damage)
- Dexterity (dodge chance)
- Intelligence (magical damage)

## Status Effects

A robust status effect system will add even more nuance to the gameplay. I have some in mind already:

- Slow
- Haste
- Double damage
- Double attack speed
- Invincibility
- Double vulnerability
- Polymorph (replaces commands with animal sounds)
- Extra damage type vulnerability
- Poison/Damage over time

## Characters

### Hank - The Survivor

Hank sees himself as a VIP. Therefore, he shouldn't have to lift a finger. He does enough work running to and fro and bossing his companions around.

### Brynhilda

The starting companion. She wields an axe and begrudgingly does Hank's bidding day in and day out.

### Sammy

Sammy is a shield-bearer who doesn't say much. He is well-rounded and high utility. He's along for the ride.

### Felix

Felix is a rogueish crossbow wielder who's only in it for the coin. Hank delays the truth about the coin every way he can.

### Diego

Diego is athletic and intense. He likes long bar fights on the beach and doesn't need to be asked twice to manhandle Hank.

### Canine

Canine is a feral woman who doesn't take commands well but has high stats and hidden potential.

### Celosia's Guest Character

TBD

## Synergies

I won't have anything for this until I start fleshing out gameplay logic for stats, status effects, and characters, but synergies are very important for this kind of game and add most of the replay value.