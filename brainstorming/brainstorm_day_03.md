# Brainstorm - Day 3

I didn't do much yesterday, but I plan to make up for it today. I think I've got enough stats, so next I'll pare those down to a nice simple spread for the game. Maybe I can add more in after the game jam.

Then solidifying a nice simple spread of status effects would be useful. Finally, it's time to start fleshing out the companions by drawing their attack patterns, defining their base stats, listing their command menus, and beginning work on their upgrade paths.

I hope to be basically done planning companion information today (there is time to finish their command lists later on) and then I want to model their branching upgrade paths tomorrow. I also want to list meta progressions for the survivor tomorrow.

## Bob the Kob

Celosia gave me the last companion; Bob the Kobold. Import notes on him later. He can be the metaprogression merchant who becomes a companion when you upgrade enough or perhaps after buying useless upgrades enough.

## Status Effects

We need to get all the status effects figured out before doing stats, as there will be stats related to status effects. Here's the list so far:

- Slow
- Haste
- Double damage
- Double attack speed
- Invincibility
- Double vulnerability
- Polymorph (replaces survivor's commands with animal sounds and makes you slower)
- Extra damage type vulnerability
- Poison/Damage over time

And here are the new ones:

- Petrified
- Charm (use allegiance flag so this easily works on all entities?)
- Burn (damage over time)
- Electrified (hurts nearby allies when moving)
- Chilled (slow and lowered attack speed)

## Statistics

I'm going back through and using **bold** and ~~strikethrough~~ to mark what's definitely staying in and what's definitely getting cut.

Existing statistics:

- **Attack speed**
- **Movement speed**
- **Critical chance**
- **Critical damage multiplier**
- **Additional attack (see Halls of Torment)**
- ~~Projectile amount~~
- **Attack area size**
- ~~Projectile area size~~
- **Projectile speed**
- **Projectile lifetime**
- **Effect chance**
- **Strength**
- **Dexterity**
- **Intelligence**
- **Knockback**
- **HP**
- **Energy**
- **Experience Level**
- **Physical defense**
- **Magical defense**
- ~~Block strength (see Halls of Torment)~~
- **Health regeneration**
- **Range**
- ~~Duration (ultimates/commands/summons?)~~
- **Experience gain multiplier**
- **Cooldown reduction**
- **Luck**
- **Curse (see Vampire Survivors)**
- **Rerolls**
- **Extra lives**

And the new ones:

- **Fire resistance**
- **Lightning resistance**
- **Frost resistance**
- **Poison resistance**
- **Might (see Vampire Survivors)**
- **Defense bonus**
- **Effect duration**
- **Physical attack**
- **Magical attack**
- **Constitution**
- **Wisdom**
- **Knockback resistance**
- **Energy regeneration**
- **Dodge chance**

Now here are two new lists. One is the "keeper" stats which are independent, and the other has the ones which are derived from or used to derive others and their relationships.

**Independent stats**:

- Strength
- Dexterity
- Intelligence
- Constitution
- Wisdom
- Luck (slightly increases every stat by a factor of luck)
- Might
- Defense bonus
- Curse
- Rerolls
- Extra Lives
- Attack area size
- Projectile speed
- Energy
- Experience level

**Derived stats**:

- Attack speed (base + factor of Dexterity)
- Movement speed (base + factor of Dexterity)
- Critical chance (base + factor of Dexterity + extra factor of Luck)
- Critical damage multiplier (base + extra factor of Luck)
- Additional attack (base + factor of Dexterity)
- Projectile lifetime (base + factor of Intelligence)
- Effect chance (base + factor of Dexterity + factor of Intelligence)
- Knockback (base + factor of Strength)
- HP (base + factor of Constitution)
- Physical defense (base + factor of Constitution)
- Magical defense (base + factor of Wisdom)
- Health regeneration (base + factor of Constitution + factor of Wisdom)
- Range (base + factor of Dexterity + factor of Intelligence)
- Experience gain multiplier (base + extra factor of Luck)
- Cooldown reduction (base + factor of Intelligence + factor of Wisdom)
- Fire resistance (base + factor of Constitution + factor of Wisdom)
- Lightning resistance (base + factor of Constitution + factor of Wisdom)
- Frost resistance (base + factor of Constitution + factor of Wisdom)
- Poison resistance (base + factor of Constitution + factor of Wisdom)
- Effect duration (base + factor of Dexterity + factor of Intelligence)
- Physical attack (base + factor of Strength)
- Magical attack (base + factor of Intelligence)
- Knockback resistance (base + factor of Constitution)
- Energy regeneration (base + factor of Dexterity)
- Dodge chance (base + factor of Dexterity)

## Base Stats

Here is where we figure out what every character (survivor and companions) should have for their base stats. We can start abstractly since there is no defined scale for stats yet. Every character gets a section for every stat and every stat has a bar that is filled with up to six segments.

I'll be doing this in spreadsheet software and hopefully linking to the pages from here. Experience level in the spreadsheet can be used later for a formula to see what the stats are like at higher levels. I have to figure out how they change over level first.

## Command Lists

Look at FFXII and Dragon's Dogma for inspiration.

### Shared

- Stay Close: Toggles with Keep Your Distance, orders companion to stay close
- Keep Your Distance: Toggles with Stay Close, orders companion to stay further away
- Prioritize Weaker Enemies: Toggles with Prioritize Stronger Enemies, orders companion to go after enemies with lower level first
- Prioritize Stronger Enemies: Toggles with Prioritize Weaker Enemies, orders companion to go after enemies with higher level first
- Take The Initiative: Toggles on or off, orders companion to automatically use eligible unique commands or not (settings menu for each companion to decide which commands are affected by this)
- Heal Me: Toggles between off and different thresholds to heal at, orders companion to use healing commands when they can at certain thresholds

### Brynhilda

- Cleave: Temporary buff, enhanced attack area size (and attack speed?) for self
- Shieldmaiden: Temporary buff, enhanced defense stats for survivor and enhanced knockback resistance for survivor and companions
- 